-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-04-19 11:47:38
-- 服务器版本： 5.7.9
-- PHP Version: 5.5.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `www`
--

-- --------------------------------------------------------

--
-- 表的结构 `t_tasks`
--

CREATE TABLE `t_tasks` (
  `uid` int(11) NOT NULL,
  `title` text,
  `time` text,
  `finish_time` text,
  `finish` int(100) DEFAULT '0',
  `address` text,
  `reason` text,
  `result` text,
  `phone` text,
  `number` text,
  `student_name` text,
  `finish_name` text,
  `token` text,
  `progress` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_tasks`
--

INSERT INTO `t_tasks` (`uid`, `title`, `time`, `finish_time`, `finish`, `address`, `reason`, `result`, `phone`, `number`, `student_name`, `finish_name`, `token`, `progress`) VALUES
(33, 'span 未生效的属性', '2016-04-09 18:02:25.031', '', 0, 'Swift Street', '', '', '18865273903', '372321199609098082', 'Domains Hi', '', '28c64970-fe3a-11e5-8d1d-b9aebbae241b', ''),
(1, '12', '2', '2', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `t_tips`
--

CREATE TABLE `t_tips` (
  `uid` int(11) NOT NULL,
  `title` text CHARACTER SET utf8,
  `content` text CHARACTER SET utf8,
  `time` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `t_tips`
--

INSERT INTO `t_tips` (`uid`, `title`, `content`, `time`) VALUES
(1, '2', 's', 'ss'),
(2, 'span 未生效的属性', '<p>werytui</p>', '2016-04-09 19:41:17.276'),
(3, 'span 未生效的属性问问', '<p>werytuitry 45e打四分黑二哥二二</p>', '2016-04-09 19:41:59.758');

-- --------------------------------------------------------

--
-- 表的结构 `t_users`
--

CREATE TABLE `t_users` (
  `uid` int(11) NOT NULL,
  `username` text,
  `password` text,
  `phone` text,
  `nickname` text,
  `qq` text,
  `hotel` text,
  `clazz` text,
  `finish_time` text,
  `hometown` text,
  `token` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_users`
--

INSERT INTO `t_users` (`uid`, `username`, `password`, `phone`, `nickname`, `qq`, `hotel`, `clazz`, `finish_time`, `hometown`, `token`) VALUES
(1, 'www', '4eae35f1b35977a00ebd8086c259d4c9', '18865273903', '正在游戏中', '1938668806', '学院5号楼123宿舍', '2012级5班', '2', '临沂蒙阴', '1460198049787');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_tasks`
--
ALTER TABLE `t_tasks`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `t_tips`
--
ALTER TABLE `t_tips`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`uid`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `t_tasks`
--
ALTER TABLE `t_tasks`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- 使用表AUTO_INCREMENT `t_tips`
--
ALTER TABLE `t_tips`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 使用表AUTO_INCREMENT `t_users`
--
ALTER TABLE `t_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
