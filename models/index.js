var db = require('../config/mysql');
var _ = require('underscore');

var Index = function() {};


//提交
Index.prototype.submit = function(id, callback) {
   //这里获取到所有要插入数据库的值 id
   //标题 时间 地址 缘由 电话 学号 姓名
    //console.log(id);
    var sql = "INSERT INTO `t_tasks` (`title`, `time`, `address`, `reason`, `phone`, `number`, `student_name`,`token`) VALUES (?,?,?,?,?,?,?,?);";
    // get a connection from the pool
    //console.log(sql);
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//token
Index.prototype.token = function(id, callback) {
  //id  是 token

    var sql = "SELECT `title`,`time`,`finish_time`,`finish`,`address`,`reason`,`phone`,`number`,`result`,`student_name`,`finish_name` ,`token` FROM `t_tasks` WHERE `token` = ?;";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};


//tips
Index.prototype.tips = function(id, callback) {
    //console.log(id);
    var sql = "SELECT `uid`, `title`, `content`, `time` FROM `t_tips` limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};


//tip
Index.prototype.tip = function(id, callback) {
    //console.log(id);
    var sql = "SELECT `title`, `content`, `time` FROM `t_tips` where `uid` = ?";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};



//Index
Index.prototype.show = function(id, callback) {
  //后台首页展示最新的十个
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`finish`,`address`,`reason`,`phone`,`number`,`result`,`student_name`,`finish_name` FROM `t_tasks`  limit 10";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//show not finish
Index.prototype.shownotfinish = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks` WHERE `finish` = 0  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//show finish
Index.prototype.showfinish = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks` WHERE `finish` = 1  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};


module.exports = Index;
