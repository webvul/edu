var db = require('../config/mysql');
var _ = require('underscore');

var User = function() {};


//login
User.prototype.login = function(id, callback) {
  //登陆
    var sql = "SELECT `uid` FROM `t_users` WHERE `username`= ? and `password` = ?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//add user
User.prototype.add = function(id, callback) {
    var sql = "INSERT INTO `t_users` ( `username`, `password`, `phone`, `nickname`, `qq`, `hotel`, `clazz`, `hometown`) VALUES (?,?,?,?,?,?,?,?);";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//delete user
User.prototype.delete = function(id, callback) {
    var sql = "DELETE FROM `t_users` WHERE `uid` = ?;";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//show user
User.prototype.user = function(id, callback) {
    var sql = "SELECT `uid`, `username`, `password`, `phone`, `nickname`, `qq`, `hotel`, `clazz`, `finish_time`, `hometown`, `token` FROM `t_users` WHERE  `uid` = ?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//change passwd
User.prototype.passwd = function(id, callback) {
  //改变密码
    var sql = "UPDATE `t_users` SET `password`= ?  WHERE  `uid`= ?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};


//change info
User.prototype.info = function(id, callback) {
  //改变信息
    var sql = "UPDATE `t_users` SET `phone`=?,`nickname`=?,`qq`=?,`hotel`=?,`clazz`=?,`hometown`=? WHERE  `uid`= ?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};





//details
User.prototype.details = function(id, callback) {
  //后台根据uid 查看报修任务
    var sql = "SELECT `title`,`time`,`finish_time`,`finish`,`address`,`reason`,`phone`,`number`,`result`,`student_name`,`finish_name`,`token` FROM `t_tasks` WHERE `uid` = ?;";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//index
User.prototype.index = function(id, callback) {
  //后台首页展示最新的十个
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`finish`,`address`,`reason`,`phone`,`number`,`result`,`student_name`,`finish_name` FROM `t_tasks`  limit 10";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};


// do finish
User.prototype.finish = function(id, callback) {
    console.log(id);
    var sql = "UPDATE `t_tasks` SET `finish_time`=?,`finish`=?,`result`=?,`finish_name`=? WHERE `uid` = ? ";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//show not finish
User.prototype.shownotfinish = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks` WHERE `finish` = 0  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
          console.log(err);
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
              console.log(err);
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//show all
User.prototype.showall = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks`  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//show finish
User.prototype.showfinish = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks` WHERE `finish` = 1  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//add tips
User.prototype.addtips = function(id, callback) {
    console.log(id);
    var sql = "INSERT INTO `t_tips` (`title`,`content`,`time`) VALUES (?,?,?);";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
          console.log(err);
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
              console.log(err);
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//change tips
User.prototype.changetips = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks` WHERE `finish` = 1  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};

//delete tips
User.prototype.deletetips = function(id, callback) {
    console.log(id);
    var sql = "SELECT `uid`,`title`,`time`,`finish_time`,`address`,`reason`,`result`,`phone`,`number`,`student_name`,`finish_name` FROM `t_tasks` WHERE `finish` = 1  limit 10";
    console.log(sql);
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, id, function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            callback(false, results);
        });
    });
};




module.exports = User;
