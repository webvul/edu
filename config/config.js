module.exports = {
    mysql: {
        host: '127.0.0.1',
        user: 'root',
        password: 'toor',
        database: 'www',
        connectionLimit: 10,
        supportBigNumbers: true
    },
    site: {
        url: '127.0.0.1:3000',
        name: 'LCU Nic Center'
    }

};
