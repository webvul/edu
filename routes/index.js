var express = require('express');
var router = express.Router();

var Index = require('../models/index');
var User = require('../models/user');
var uuid = require('node-uuid');
var md5 = require('md5');




router.get('/', function(req, res, next) {
  //跳转 index
  res.redirect('/index');
});
router.get('/index', function(req, res, next) {

      var index = new Index();
      var id= [];
      index.tips(id,function(err,result){
          if(err){
              res.render('error');
          }
          else
          {
            res.render('index', { tips: result });
          }
      });
});


//token/uuid  查看保修进度
router.get('/token', function(req, res, next) {
  var uid = req.query.uid;
  var index = new Index();
  var id= [uid];
  //console.log(id);
  //res.render('result', { result: id });

  index.token(id,function(err,result){
      if(err){
          res.render('error');
      }
      else
      {
        //console.log(result);
        res.render('token', { result: result[0] });
      }
  });
});

//submit
//学号区提交表单 入库
router.post('/submit', function(req, res, next) {
  //title`, `time`, `address`, `reason`, `phone`, `number`, `student_name

  var title = req.body.title;
  var time = new Date();
  var address = req.body.address;
  //var reason = req.body.reason;
  var phone = req.body.phone;
  var number = req.body.number;
  var student_name = req.body.student_name;
  var reason  = "";



  if(!(title && address && phone && number && student_name ))
  {
    res.render('result', { result: "参数出错<a href='/'>回到首页</a>" });
    return;
  }
  else {
    var token = uuid.v1();


    var index = new Index();
    var id= [title,time,address,reason,phone,number,student_name,token];
    //console.log(id);
    index.submit(id,function(err,result){
        if(err){
            res.render('error');
            console.log(err);
        }
        else
        {
          console.log(result);
          res.render('submit', { result: token });
        }
    });


    //res.render('index', { title: 'Express' });
  }


});



//login
router.post('/login', function(req, res, next) {
  var username = req.body.username;
  var password = md5(req.body.password);
  var user = new User();
  var id= [username , password];
  console.log(id);
  user.login(id,function(err,result){
      if(err){
        console.log(err);
          res.render('error');
      }
      else
      {
        res.render('result', { result: "登陆成功 , 正在跳转 <script>window.location.href='/user/';</script>" });
      }
  });
});

//login
router.get('/login', function(req, res, next) {
  res.render('login');
});

//chat
router.post('/chat', function(req, res, next) {
  var nickname = req.body.nickname;
  console.log(nickname);
  res.render('chat', { nickname: nickname });
});


//logout
router.get('/logout', function(req, res, next) {

  //在这里销毁session

  res.render('result', { result: "退出成功 , 正在跳转首页 <script>window.location.href='/';</script>" });
});


//show tips
router.get('/tips/:uid', function(req, res, next) {
  var uid = req.params.uid;
  var index = new Index();
  var id= [uid];
  //console.log(id);
  index.tip(id,function(err,result){
      if(err){
          res.render('error');
      }
      else
      {
        //console.log(result[0]['title']);
        if(result[0] && result[0]['title'] != "")
        {
          res.render('tips', { tip: result[0] });
        }
        else {
          res.render('error');
        }
      }
  });
});

module.exports = router;
